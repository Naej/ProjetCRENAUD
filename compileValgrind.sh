#!/bin/sh

cd build
cmake ..
make
cd ..
valgrind --leak-check=full --leak-resolution=high --show-reachable=yes --track-origins=yes ./build/bin/Programme
