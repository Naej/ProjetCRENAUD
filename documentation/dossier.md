% Projet CRENAUD : Catch Royal Edition Nazi : An Ultimate Dictator
% Bartuccio Antoine \cr Amalvy Arthur \cr Abbe Magsen
% Printemps 2016

\newpage

# Présentation du projet

## Fonctionnement

Le principe de fonctionnement est très simple. Il faut embaucher des catcheurs en respectant le budget disponible pour une année. Il faut après organiser les matchs de manière a rapporter le plus de popularité pour arriver en tête du classement. Un match se déroule en deux parties : on sélectionne le gagnant puis le perdant. Cependant, il faut faire attention aux status affectant les catcheurs. En effet, ceux-ci peuvent influencer les résultats du match et faire perdre de l'argent à l'utilisateur. De plus, lorsqu'un catcheur combat, il gagne automatiquement un status aléatoire à cause des risques de cette pratique sportive. À la fin de chaque années, la position de la ligue de catch de l'utilisateur par rapport à ses adversaires est affichée. Le but est d'être premier au classement. Si l'objectif n'est pas accompli, une nouvelle année commence et il est possible de sélectionner un nouveau groupe de catcheur.

## Le contexte

Par soucis de cohérence, tous les événements relatés dans ce logiciel se déroulent dans un monde parallèle. D’ailleurs, dans cet univers, les années du calendrier sont nommées à partir de nom d'animaux et celui-ci est fournit dans ce dossier.

![Nommage des années du calendrier](Calendrier.png)

Pour mieux comprendre la situation de cet univers, nous vous proposons un bref historique des événements s'y étant déroulées.

## L'histoire

En raison de l'utilisation d'un calendrier différent dans l'univers parallèle, nous allons ici replacer la chronologie mondiale dans notre calendrier civil plus compréhensible pour nous qui sommes étrangers à ce monde.

* TERRE, ANNEE 1941, 2^nd^ guerre mondiale, le Gouvernement japonais s'accorde avec les états-unis afin que ceux-ci n'entre pas dans le conflit.
* 1946, utilisation des deux premières bombes nucléaire allemande sur Paris et Londres et effondrement en quelques mois de l'Europe résistante. Exode massive des occidentaux vers L'Afrique suite à la fermeture des frontières américaines.
* 1948, Début des conflits sino-soviétique.
* 1950, Annexion par les États-Unis du Canada et de l'Amérique du Sud.
* 1952, Création de la Nouvelle France suite à la réunification des anciennes colonies Françaises et l'appui des anciennes puissances occidentales nouveaux colons. Des conflits de territoires éclatent à travers L'Afrique.
* 1960, Reddition et Partage de la Russie entre Le Reich et les nippons.
* 1962, Coup d'état de Napoléon Malaparte. Grande réussite militaire et politique.
* 1963/64, Arrivé aux pouvoirs des maréchaux Guillaume le goéland et Alexandre le plus grand et mise en place d'une stratégie d'annexion de l'Afrique. Avec Napoléon, ils constituent les trois pères fondateurs de la Nouvelle France.
* 1965, mise en place des prequels de la politique de divertissements massifs. Introduction des lois Memef.
* 1970, Afrique sous le joug du nouvel empire français. Disparition des trois combattants français. Ébranlement du système français, moteur du monde d'après guerre.
* 1971, grande crise pétrolière. Grands embargos commerciaux contre la nouvelle France.
* 1974, mise en place de politique de divertissements massifs à travers la planète pour calmer les populations.
* 1975, effondrement des puissances politiques, augmentation de la population mondiale, enrichissement croissant des firmes mondiales. Investissement des ligues de catch dans des sociétés annexes.
* 1980, les ligues possède 80% des placements dans la production de blé, d'orge, de riz et de mais, 70% des placements dans l'informatique et les médias, 90% dans l'agroalimentaire. Le fond d'investissement des ligues représente 78.36% des richesses mondiales.
* 1982, première élection durant laquelle une ligue de catch est mise au pouvoir.
* 1990, Création de la ligue mondiale de Catch. Compétition ou chaque ligue peut participer. Le vainqueur peut prétendre aux droits de politique mondiale durant un an.
* 2000/20, la ligue Nazi n'a pas subis une seule défaite.
* 2003, découverte d'un médicament miracle, régularisation de ce médicament uniquement pour le catch afin d'éviter un épuisement des ressources mondiales.
* 2004, premier dispositif de résurrection, loi Terci, interdisant l'utilisation de ce dispositif, avec comme exception l'utilisation sur un catcheur.
* 2010, mise en place des lois de réglementations des tournois de catch libre.
* 2015, Constitution mondiale de Catch.
* 2016, Déclaration des Droits de l'Homme à l'accès au Catch.