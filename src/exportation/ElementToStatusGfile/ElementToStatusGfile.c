/*
* @Author: klmp200
* @Date:   2016-05-27 09:20:31
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-27 09:22:08
*/

#include <stdio.h>
#include <stdlib.h>

#include "../../lists/list.h"
#include "../../status/status.h"
#include "../StatusToGfile/StatusToGfile.h"

S_gfile * ElementToStatusGfile(S_element *element){

	return StatusToGfile((S_status*)element->data);

}