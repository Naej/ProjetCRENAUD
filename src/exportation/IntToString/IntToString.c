/*
* @Author: klmp200
* @Date:   2016-05-18 22:23:35
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-18 22:56:37
*/

#include <stdio.h>
#include <stdlib.h>

char * IntToString(int number){

	char *string = NULL;
	int tmp = number;
	int size = 1;

	if (number < 0){

		size ++;
	}

	while (tmp != 0){

		tmp = tmp / 10;
		size++;

	}

	string = malloc(sizeof(char) * size);

	if (string != NULL){

		sprintf(string, "%d", number);

	}

	return string;
}
