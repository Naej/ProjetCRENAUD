/*
* @Author: klmp200
* @Date:   2016-05-26 14:34:20
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-28 01:22:45
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../../player/player.h"
#include "../../gyaml/gyaml.h"
#include "../SetKeyExport/SetKeyExport.h"
#include "../SetDataExport/SetDataExport.h"
#include "../IntToString/IntToString.h"

S_gfile * PlayerToGfile(S_player * player){
	S_gfile *gfile = InitiateGfile();
	S_gyaml gyaml;
	char *integer = NULL;

	if (SetSizeGfile(gfile, 5) == 0){


		/* Convert name */

		gyaml = InitiateGyaml();
		SetKeyExport(&gyaml, "name", 5);
		SetDataExport(&gyaml, player->name);
		memcpy(&gfile->data[0], &gyaml, sizeof(S_gyaml));

		/* Convert money */

		gyaml = InitiateGyaml();
		SetKeyExport(&gyaml, "money", 6);
		integer = IntToString(player->money);
		SetDataExport(&gyaml, integer);
		free(integer);
		memcpy(&gfile->data[1], &gyaml, sizeof(S_gyaml));

		/* Convert fame */

		gyaml = InitiateGyaml();
		SetKeyExport(&gyaml, "fame", 5);
		integer = IntToString(player->fame);
		SetDataExport(&gyaml, integer);
		free(integer);
		memcpy(&gfile->data[2], &gyaml, sizeof(S_gyaml));

		/* Convert current Year */

		gyaml = InitiateGyaml();
		SetKeyExport(&gyaml, "current year", 13);
		integer = IntToString(player->currentYear);
		SetDataExport(&gyaml, integer);
		free(integer);
		memcpy(&gfile->data[3], &gyaml, sizeof(S_gyaml));

		/* Convert current Round */

		gyaml = InitiateGyaml();
		SetKeyExport(&gyaml, "current round", 14);
		integer = IntToString(player->currentRound);
		SetDataExport(&gyaml, integer);
		free(integer);
		memcpy(&gfile->data[4], &gyaml, sizeof(S_gyaml));


	}

	return gfile;
}