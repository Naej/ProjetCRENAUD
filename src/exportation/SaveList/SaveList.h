#ifndef SAVELIST_H_INCLUDED
#define SAVELIST_H_INCLUDED

#include "../../lists/list.h"
#include "../../gyaml/gyaml.h"
#include "../../importation/importation.h"
#include "../ElementToStatusName/ElementToStatusName.h"
#include "../ElementToWrestlerGfile/ElementToWrestlerGfile.h"
#include "../ElementToStatusGfile/ElementToStatusGfile.h"

#define SaveWrestlers(path, list) (SaveList(path, list, ElementToWrestlerName, ElementToWrestlerGfile))
#define SaveStatus(path, list) (SaveList(path, list, ElementToStatusName, ElementToStatusGfile))

int SaveList(char[], S_list*, char *(*)(S_element *), S_gfile *(*)(S_element *));

/*
	Save a list of wrestlers in a given folder
	Return 0 in case of success and 1 if it fails
*/

#endif // SAVELIST_H_INCLUDED