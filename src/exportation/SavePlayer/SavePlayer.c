/*
* @Author: klmp200
* @Date:   2016-05-26 16:29:40
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-26 16:35:08
*/

#include <stdio.h>
#include <stdlib.h>

#include "../../player/player.h"
#include "../../gyaml/gyaml.h"
#include "../PlayerToGfile/PlayerToGfile.h"

int SavePlayer(char path[], S_player player){

	int ok = 1;
	S_gfile *gfile = PlayerToGfile(&player);

	ok = WriteGfile(path, gfile);

	FreeGfile(gfile);

	return ok;
}