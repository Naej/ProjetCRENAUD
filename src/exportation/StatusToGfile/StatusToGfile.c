/*
* @Author: klmp200
* @Date:   2016-05-19 10:05:29
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-21 19:26:45
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../../status/status.h"
#include "../../gyaml/gyaml.h"
#include "../SetKeyExport/SetKeyExport.h"
#include "../SetDataExport/SetDataExport.h"
#include "../IntToString/IntToString.h"

S_gfile * StatusToGfile(S_status *status){
	S_gfile *gfile = InitiateGfile();
	S_gyaml gyaml;
	char *nbString = NULL;

	if (SetSizeGfile(gfile, 6) == 0){

		/* Convert name */

		gyaml = InitiateGyaml();
		SetKeyExport(&gyaml, "name", 5);
		SetDataExport(&gyaml, status->name);
		memcpy(&gfile->data[0], &gyaml, sizeof(S_gyaml));

		/* Convert mMoney */

		gyaml = InitiateGyaml();
		SetKeyExport(&gyaml, "malus money", 12);
		nbString = IntToString(status->mMoney);
		SetDataExport(&gyaml, nbString);
		free(nbString);
		memcpy(&gfile->data[1], &gyaml, sizeof(S_gyaml));

		/* Convert bFame */

		gyaml = InitiateGyaml();
		SetKeyExport(&gyaml, "bonus fame", 11);
		nbString = IntToString(status->bFame);
		SetDataExport(&gyaml, nbString);
		free(nbString);
		memcpy(&gfile->data[2], &gyaml, sizeof(S_gyaml));

		/* Convert bStrength */

		gyaml = InitiateGyaml();
		SetKeyExport(&gyaml, "bonus strength", 15);
		nbString = IntToString(status->bStrength);
		SetDataExport(&gyaml, nbString);
		free(nbString);
		memcpy(&gfile->data[3], &gyaml, sizeof(S_gyaml));

		/* Convert rTime */

		gyaml = InitiateGyaml();
		SetKeyExport(&gyaml, "remaning time", 17);
		nbString = IntToString(status->rTime);
		SetDataExport(&gyaml, nbString);
		free(nbString);
		memcpy(&gfile->data[4], &gyaml, sizeof(S_gyaml));

		/* Convert sideEffect */

		gyaml = InitiateGyaml();
		SetKeyExport(&gyaml, "side effect", 12);
		if (status->sideEffect != NULL){

			if (status->sideEffect->name != NULL){

				SetDataExport(&gyaml, status->sideEffect->name);

			} else {

				SetDataExport(&gyaml, "");

			}

		} else {

			SetDataExport(&gyaml, "");

		}
		memcpy(&gfile->data[5], &gyaml, sizeof(S_gyaml));

	}

	return gfile;
}