#ifndef STATUSTOGFILE_H_INCLUDED
#define STATUSTOGFILE_H_INCLUDED

#include "../../status/status.h"
#include "../../gyaml/gyaml.h"

S_gfile * StatusToGfile(S_status *);

/*
	Convert a status into a gfile
	Return null in case of failure
*/

#endif // STATUSTOGFILE_H_INCLUDED