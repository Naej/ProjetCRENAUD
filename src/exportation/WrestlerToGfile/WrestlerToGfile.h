#ifndef WRESTLERTOGFILE_H_INCLUDED
#define WRESTLERTOGFILE_H_INCLUDED

#include "../../wrestler/wrestler.h"
#include "../../gyaml/gyaml.h"

S_gfile * WrestlerToGfile(S_wrestler *);

/*
	Convert a wrestler into a gfile
	Return null in case of failure
*/

#endif // WRESTLERTOGFILE_H_INCLUDED