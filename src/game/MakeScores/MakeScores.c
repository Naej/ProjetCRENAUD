/*
* @Author: klmp200
* @Date:   2016-06-01 21:35:18
* @Last Modified by:   klmp200
* @Last Modified time: 2016-06-02 10:05:31
*/

#include <stdio.h>
#include <stdlib.h>

#include "../../ui/ui.h"
#include "../../player/player.h"
#include "../S_game.h"

void MakeScores(S_game *game){

	int i;
	S_player *tmp = NULL;

	S_player createCompet[] = {

		InitiatePlayer("Nouvelle France Officielle", 8, 0),
		InitiatePlayer("Democratie", 8, 1),
		InitiatePlayer("Japon", 789789, 6788),
		InitiatePlayer("WWWWEE", 8799, 6799),
		InitiatePlayer("Trump", 89787, 7899),
		InitiatePlayer("Mexicaine", 8798, 81234),
		InitiatePlayer("Nazi", 66600, 87978),

	};

	S_player *compet[MAX_COMPET];

	compet[0] = &game->player;

	for (i = 1; i < MAX_COMPET; i++){

		compet[i] = &createCompet[i - 1];

	}

	i = 1;

	while (i < MAX_COMPET && compet[i-1]->fame > compet[i]->fame){

		tmp = compet[i];
		compet[i] = compet[i-1];
		compet[i-1] = tmp;

		i++;

	}

	if (compet[MAX_COMPET - 1] == &game->player){

		game->victory = 1;

	}

	DisplayScoring(compet);

}