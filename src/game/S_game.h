#ifndef SGAME_H_INCLUDED
#define SGAME_H_INCLUDED

#include "../lists/list.h"
#include "../player/player.h"

typedef struct S_game S_game;
struct S_game {
	S_list *wList;
	S_list *sList;
	S_player player;
	int mode;
	int save;
	int victory;
};

/*
	modes :
		1 arcade
		0 adventure
	save :
		-1 exit without save
		1 want to save
		0 doesn't want to quit
	victory :
		1 player won
		0 player have not won yet
*/

#endif // SGAME_H_INCLUDED