/*
* @Author: klmp200
* @Date:   2016-05-11 15:58:13
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-26 15:35:56
*/

#include <stdio.h>
#include <stdlib.h>

#include "../S_gyaml.h"
#include "../S_gfile.h"
#include "../FreeGyaml/FreeGyaml.h"

int FreeGfile(S_gfile * gfile) {
	int ok = 1;
	int i;

	if (gfile != NULL){

		if (gfile->data != NULL){

			for (i = 0; i < gfile->size; i++){
				FreeGyaml(&gfile->data[i]);
			}

			free(gfile->data);
		}

		free(gfile);

		ok = 0;
	}

    return ok;
}