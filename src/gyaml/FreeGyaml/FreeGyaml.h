#ifndef FREEGYAML_H_INCLUDED
#define FREEGYAML_H_INCLUDED

#include "../S_gyaml.h"

void FreeGyaml(S_gyaml *);

/*
	Free content of a gyaml struct
*/

#endif // FREEGYAML_H_INCLUDED