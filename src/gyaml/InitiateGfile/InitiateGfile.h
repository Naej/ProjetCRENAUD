#ifndef INITIATEGFILE_H_INCLUDED
#define INITIATEGFILE_H_INCLUDED

#include "../S_gfile.h"

S_gfile * InitiateGfile();

/*
	Initiate a gfile and set all data to default
*/

#endif // INITIATEGFILE_H_INCLUDED