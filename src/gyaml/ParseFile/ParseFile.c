/*
* @Author: klmp200
* @Date:   2016-05-07 01:15:30
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-18 19:50:01
*/

#include <stdio.h>
#include <stdlib.h>

#include "../../lists/list.h"
#include "../ParseLineGyaml/ParseLineGyaml.h"
#include "../InitiateGyaml/InitiateGyaml.h"
#include "../S_gyaml.h"
#include "../S_gfile.h"
#include "../InitiateGfile/InitiateGfile.h"
#include "../SetSizeGfile/SetSizeGfile.h"
#include "../FreeGyaml/FreeGyaml.h"
#include "../FreeGfile/FreeGfile.h"

S_gfile * ParseFile(char path[]){
	int ok = 1;
	int stillData = 1;
	int i;

	FILE *file = fopen(path, "r");
	S_gyaml collected = InitiateGyaml();
	S_list *list = InitiateList();
	S_gfile *gfile = InitiateGfile();
	S_element *element = NULL;

	if (file != NULL){

		while (stillData == 1 && ok != -1){
			do {
				ok = ParseLineGyaml(file, &collected);
			} while (ok == 0);

			if (ok == 1){
				AppendList(list, &collected, sizeof(collected));
				collected = InitiateGyaml();
				ok = 0;
			} else {
				if (collected.key == NULL){
					SetSizeGfile(gfile, list->size);

					element = list->first;
					i = 0;
					while (element != NULL){
						gfile->data[i] = *((S_gyaml*)(element->data));
						element = element->next;
						i++;
					}
					FreeList(list);
					stillData = 0;
				}
			}
		}
		fclose(file);

	} else {
		FreeGfile(gfile);
		gfile = NULL;
	}

	return gfile;
}