#ifndef SGYAML_H_INCLUDED
#define SGYAML_H_INCLUDED

typedef struct S_gyaml S_gyaml;
struct S_gyaml {
	char * key;
	char * data;	
	
	int keySize;
	int dataSize;
};

/*
	key and data refers to strings with unknown size
*/

#endif // SGYAML_H_INCLUDED