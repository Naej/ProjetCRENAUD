#ifndef SETSIZEGFILE_H_INCLUDED
#define SETSIZEGFILE_H_INCLUDED

#include "../S_gfile.h"

int SetSizeGfile(S_gfile *, int);

/*
	Set a gfile with the right array size
	return 0 if success and 1 if failure
*/

#endif // SETSIZEGFILE_H_INCLUDED