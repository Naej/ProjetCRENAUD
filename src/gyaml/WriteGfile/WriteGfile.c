/*
* @Author: klmp200
* @Date:   2016-05-19 15:44:02
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-27 14:28:49
*/

#include <stdio.h>
#include <stdlib.h>

#include "../S_gfile.h"
#include "../S_gyaml.h"
#include "../WriteGyamlKey/WriteGyamlKey.h"
#include "../WriteGyamlData/WriteGyamlData.h"

int WriteGfile(char path[], S_gfile *gfile){
	FILE *file = fopen(path, "w+");
	int ok = 1;
	int i;

	if (gfile != NULL && file != NULL){

		for (i=0;i<gfile->size;i++){

			if (gfile->data[i].data != NULL && gfile->data[i].data[0] != '\0'){

				WriteGyamlKey(file, gfile->data[i].key);
				WriteGyamlData(file, gfile->data[i].data);

			}

		}

		ok = 0;
	}

	if (file != NULL){

		fclose(file);

	}

	return ok;
}