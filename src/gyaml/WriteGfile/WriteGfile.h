#ifndef WRITEGFILE_H_INCLUDED
#define WRITEGFILE_H_INCLUDED

#include "../S_gfile.h"

int WriteGfile(char[], S_gfile *);

/*
	Write a gfile into a given file
	Gfile will not be clear
	Given file will be overitten
	Return 0 in case of success and 1 in case of failure
*/

#endif // WRITEGFILE_H_INCLUDED