/*
* @Author: klmp200
* @Date:   2016-05-19 16:29:25
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-20 09:29:28
*/

#include <stdio.h>
#include <stdlib.h>

int WriteGyamlData(FILE *file, char *string){
	int ok = 1;
	int i = 0;

	if (file != NULL && string != NULL){

		if (string[i] != '\0'){

			fputc('	', file);

		}

		while (string[i] != '\0'){

			fputc(string[i], file);
			if (string[i] == '\n' && string[i+1] != '\0'){

				fputc('	', file);

			}
			i++;
		}
		fputc('\n', file);
		ok = 0;
	}

	return ok;
}