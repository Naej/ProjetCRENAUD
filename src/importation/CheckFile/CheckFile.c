/*
* @Author: klmp200
* @Date:   2016-05-25 22:53:39
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-25 22:55:43
*/

#include <stdio.h>
#include <stdlib.h>

int CheckFile(char path[]){

	int ok = 0;
	FILE *file = NULL;

	file = fopen(path, "r");

	if (file != NULL){
		ok = 1;
		fclose(file);
	}

	return ok;
}