#ifndef ENDATLAST_H_INCLUDED
#define ENDATLAST_H_INCLUDED

void EndAtLast(char *, int);

/*
	End string at the last char given
*/

#endif // ENDATLAST_H_INCLUDED