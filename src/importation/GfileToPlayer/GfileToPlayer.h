#ifndef GFILETOPLAYER_H_INCLUDED
#define GFILETOPLAYER_H_INCLUDED

#include "../../player/player.h"
#include "../../gyaml/gyaml.h"

S_player GfileToPlayer(S_gfile *);

/*
	Convert a gfile into a player
*/

#endif // GFILETOPLAYER_H_INCLUDED