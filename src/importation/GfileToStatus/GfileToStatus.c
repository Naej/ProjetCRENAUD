/*
* @Author: klmp200
* @Date:   2016-05-18 11:42:31
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-20 09:53:03
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../ReplaceInString/ReplaceInString.h"
#include "../EndAtLast/EndAtLast.h"
#include "../../status/status.h"
#include "../../gyaml/gyaml.h"

S_status GfileToStatus(S_gfile * gfile){
	S_status status = InitiateStatus();
	int i;

	for(i=0; i < gfile->size;i++){

		if(strcmp(gfile->data[i].key, "name") == 0){

			status.name = gfile->data[i].data;
			gfile->data[i].data = NULL;

			EndAtLast(status.name, '\n');
			ReplaceInString(status.name, '\n', ' ');

		} else if (strcmp(gfile->data[i].key, "side effect") == 0){

			status.sideEffectName = gfile->data[i].data;
			gfile->data[i].data = NULL;

			EndAtLast(status.sideEffectName, '\n');
			ReplaceInString(status.sideEffectName, '\n', ' ');

		} else if (strcmp(gfile->data[i].key, "malus money") == 0){

			status.mMoney = atoi(gfile->data[i].data);

		} else if (strcmp(gfile->data[i].key, "bonus fame") == 0){

			status.bFame = atoi(gfile->data[i].data);

		} else if (strcmp(gfile->data[i].key, "bonus strength") == 0){

			status.bStrength = atoi(gfile->data[i].data);

		} else if (strcmp(gfile->data[i].key, "remaning time") == 0){

			status.rTime = atoi(gfile->data[i].data);

		}
		FreeGyaml(&gfile->data[i]);
	}
	free(gfile->data);
	return status;
}