#ifndef GFILETOSTATUS_H_INCLUDED
#define GFILETOSTATUS_H_INCLUDED

#include "../../status/status.h"
#include "../../gyaml/gyaml.h"

S_status GfileToStatus(S_gfile *);

#endif // GFILETOSTATUS_H_INCLUDED