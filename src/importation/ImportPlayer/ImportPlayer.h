#ifndef IMPORTPLAYER_H_INCLUDED
#define IMPORTPLAYER_H_INCLUDED

#include "../../player/player.h"

S_player ImportPlayer(char[]);

/*
	Import a player from a given file
	Return an empty player if not found
*/

#endif // IMPORTPLAYER_H_INCLUDED