#ifndef IMPORTWRESTLERS_H_INCLUDED
#define IMPORTWRESTLERS_H_INCLUDED

#include "../../lists/list.h"

S_list * ImportWrestlers(char[], S_list*);

/*
	Create a list of wrestlers from a given folder
	Need a list of S_status
	Return NULL in case of failure
*/

#endif // IMPORTWRESTLERS_H_INCLUDED