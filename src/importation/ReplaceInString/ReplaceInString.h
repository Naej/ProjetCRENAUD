#ifndef REPLACEINSTRING_H_INCLUDED
#define REPLACEINSTRING_H_INCLUDED

void ReplaceInString(char[], char, char);

/*
	Need a string, a character to replace and a replacement character
	Replace a given character in a given string by an other one
*/

#endif // REPLACEINSTRING_H_INCLUDED