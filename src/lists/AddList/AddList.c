/*
* @Author: klmp200
* @Date:   2016-05-01 18:57:39
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-03 15:51:04
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../S_list.h"
#include "../S_element.h"
#include "../GetElementList/GetElementList.h"
#include "../InsertList/InsertList.h"
#include "../AppendList/AppendList.h"

int AddList(S_list *list, void *data, int size, int nb){
	int ok = 0;
	S_element *newElement = NULL;
	S_element *oldElement = NULL;

	void *newData = NULL;

	if (list != NULL){
		if(nb == 0){
			ok = InsertList(list, data, size);

		} else if(nb == list->size){
			ok = AppendList(list, data, size);

		} else if(nb < list->size){

			newElement = malloc(sizeof(*newElement));
			newData = malloc(size);

			if (newElement != NULL && newData != NULL){

				oldElement = GetElementList(list, nb);

				memcpy(newData, data, size);

				newElement->data = newData;

				newElement->previous = oldElement->previous;
				newElement->next = oldElement;

				oldElement->previous->next = newElement;
				oldElement->previous = newElement;

				list->size = list->size + 1;

			} else {
				if (newData != NULL){
					free(newData);
				}
				ok = 1;
			}

		} else {
			ok = 1;
		}

	} else {
		ok = 1;
	}

	return ok;
}