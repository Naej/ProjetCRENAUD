#ifndef ADDLIST_H_INCLUDED
#define ADDLIST_H_INCLUDED

#include "../S_list.h"

int AddList(S_list*, void*, int, int);

/*
	insert a given element into a given position in a list
	list / data / size / position
	Return 0 if success and 1 if failure
*/

#endif // ADDLIST_H_INCLUDED