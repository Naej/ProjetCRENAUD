#ifndef CONVERTTOSTRINGLIST_H_INCLUDED
#define CONVERTTOSTRINGLIST_H_INCLUDED

#include "../S_list.h"

char * ConvertToStringList(S_list *, int);

/*
	Convert a list into a string
	Need a boolean value :
		0 to keep the list
		1 to drop content
	Return NULL in case of failure
	Do not forget to free memory !!!!!
*/

#endif // CONVERTTOSTRINGLIST_H_INCLUDED