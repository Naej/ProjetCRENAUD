#ifndef CONVERTTOSTRINGLISTR_H_INCLUDED
#define CONVERTTOSTRINGLISTR_H_INCLUDED

char * ConvertToStringListR(S_list *, int);

/*
	Convert a list into a string
	Need a boolean value :
		0 to keep the list
		1 to drop content
	Return NULL in case of failure
	Do not forget to free memory !!!!!
*/


#endif // CONVERTTOSTRINGLISTR_H_INCLUDED