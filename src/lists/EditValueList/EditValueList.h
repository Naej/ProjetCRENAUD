#ifndef EDITVALUELIST_H_INCLUDED
#define EDITVALUELIST_H_INCLUDED

#include "../S_list.h"

int EditValueList(S_list *, void *, int, int);

/*
	Edit a specified value inside the list
	need list / value / size / position
	Return 0 if success and 1 if failure
*/

#endif // EDITVALUELIST_H_INCLUDED