/*
* @Author: klmp200
* @Date:   2016-05-02 18:51:39
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-02 18:54:59
*/

#include <stdio.h>
#include <stdlib.h>

#include "../S_list.h"
#include "../DropList/DropList.h"

int FreeList(S_list *list){
	int ok = 0;

	if (list != NULL){
		ok = DropList(list);
		if (ok == 0){
			free(list);
		}
	} else {
		ok = 1;
	}

	return ok;
}