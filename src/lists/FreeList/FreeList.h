#ifndef FREELIST_H_INCLUDED
#define FREELIST_H_INCLUDED

#include "../S_list.h"

int FreeList(S_list *);

/*
	Completely free a list
	Return 0 if Success and 1 if failure
*/

#endif // FREELIST_H_INCLUDED