#ifndef GETELEMENT_H_INCLUDED
#define GETELEMENT_H_INCLUDED

#include "../S_element.h"
#include "../S_list.h"

S_element * GetElementList(S_list*, int);

/*
	Get a wanted element inside the list
	Return NULL in case of failure
	Do not use it without precaution !
*/

#endif // GETELEMENT_H_INCLUDED