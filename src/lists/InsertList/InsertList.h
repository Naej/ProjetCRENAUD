#ifndef INSERTLIST_H_INCLUDED
#define INSERTLIST_H_INCLUDED

#include "../S_list.h"

int InsertList(S_list*, void*, int);

/*
	Insert void* element into a given list
	You should specify the size of the data in memory
	Return 0 if success and 1 if failure	
*/

#endif // INSERTLIST_H_INCLUDED
