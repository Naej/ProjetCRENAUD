/*
* @Author: klmp200
* @Date:   2016-04-30 15:52:31
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-06 23:03:26
*/

#include <stdio.h>
#include <stdlib.h>

#include "../S_list.h"
#include "../S_element.h"
#include "../GetElementList/GetElementList.h"
#include "../PopPtnList/PopPtnList.h"

int PopList(S_list *list, int nb){
	int ok = 0;
	S_element *toDelete = NULL;

	if (list != NULL && nb < list->size){
		toDelete = GetElementList(list, nb);

		ok = PopPtnList(list, toDelete);

	} else {
		ok = 1;
	}

	return ok;
}