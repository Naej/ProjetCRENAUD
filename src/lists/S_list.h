#ifndef SLIST_H_INCLUDED
#define SLIST_H_INCLUDED

#include "S_element.h"

typedef struct S_list S_list;
struct S_list{
	S_element *first;
	S_element *final;
	int size;
};

/*
	Struct controlling lists
*/

#endif // SLIST_H_INCLUDED