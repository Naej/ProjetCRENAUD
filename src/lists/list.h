#ifndef LIST_H_INCLUDED
#define LIST_H_INCLUDED

#include "S_list.h"
#include "S_element.h"
#include "InitiateList/InitiateList.h"
#include "InsertList/InsertList.h"
#include "GetElementList/GetElementList.h"
#include "DropList/DropList.h"
#include "PopList/PopList.h"
#include "PopPtnList/PopPtnList.h"
#include "EditValueList/EditValueList.h"
#include "GetValueList/GetValueList.h"
#include "AppendList/AppendList.h"
#include "AddList/AddList.h"
#include "FreeList/FreeList.h"
#include "ConvertToStringList/ConvertToStringList.h"
#include "ConvertToStringListR/ConvertToStringListR.h"
#include "RndList/RndList.h"

#endif // LIST_H_INCLUDED
