/*
* @Author: klmp200
* @Date:   2016-05-23 22:42:24
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-28 01:25:36
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../S_player.h"

S_player InitiatePlayer(char name[], int money, int fame){
	S_player player;

	strcpy(player.name, name);
	player.money = money;
	player.fame = fame;
	player.currentYear = 0;
	player.currentRound = 0;

	return player;
}