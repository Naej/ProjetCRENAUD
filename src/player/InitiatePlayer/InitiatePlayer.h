#ifndef INITIATEPLAYER_H_INCLUDED
#define INITIATEPLAYER_H_INCLUDED

#include "../S_player.h"

S_player InitiatePlayer(char[], int, int);

/*
	Create a new player with :
		name / money / fame
*/

#endif // INITIATEPLAYER_H_INCLUDED