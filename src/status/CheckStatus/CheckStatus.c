#include <stdio.h>
#include <stdlib.h>

#include "../S_status.h"
#include "../StatusCpy/StatusCpy.h"


S_status * CheckStatus(S_status * current)
{

	S_status *tmp = NULL;

	if(current->rTime <= 1){

		tmp = current;

		if (current->sideEffect != NULL){

			current = StatusCpy(current->sideEffect);

		} else {

			current = NULL;

		}


		free(tmp);

	} else {

		current->rTime--;

	}

	return current;

}