#ifndef FREESTATUS_H_INCLUDED
#define FREESTATUS_H_INCLUDED

#include "../S_status.h"

int FreeStatus(S_status *);

/*
	Free content of a status
	Return 0 if success and 1 if failure
*/

#endif // FREESTATUS_H_INCLUDED