#ifndef FREESTATUSLIST_H_INCLUDED
#define FREESTATUSLIST_H_INCLUDED

#include "../../lists/list.h"

int FreeStatusList(S_list *);

/*
	Free a list of status
	Return 0 if success and 1 if failure
*/

#endif // FREESTATUSLIST_H_INCLUDED