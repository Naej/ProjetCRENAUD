/*
* @Author: klmp200
* @Date:   2016-05-17 17:56:44
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-17 17:58:47
*/

#include <stdio.h>
#include <stdlib.h>

#include "../S_status.h"

S_status InitiateStatus(){
	S_status status;

	status.name = NULL;
	status.sideEffect = NULL;
	status.sideEffectName = NULL;

	status.mMoney = 0;
	status.bFame = 0;
	status.bStrength = 0;
	status.rTime = 0;

	return status;
}