/*
* @Author: klmp200
* @Date:   2016-05-22 00:22:24
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-22 01:04:43
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../S_status.h"

S_status * StatusCpy(S_status *status){
	S_status *newStatus = NULL;

	newStatus = malloc(sizeof(S_status));

	if (newStatus != NULL){

		memcpy(newStatus, status, sizeof(S_status));

	}

	return newStatus;
}