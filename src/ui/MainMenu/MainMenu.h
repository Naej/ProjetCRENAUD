#ifndef MAINMENU_H_INCLUDED
#define MAINMENU_H_INCLUDED

#include "../getcurses.h"

int MainMenu();

/*
	Display the main menu and return a choice made by the user
	Return choosen value
		-1 : quit
		2 : Credits
		3 : Quit
		
		4 : Adventure New
		5 : Adventure Load
		6 : Arcade New
		7 : Arcade Load
*/

#endif // MAINMENU_H_INCLUDED