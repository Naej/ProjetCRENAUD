#ifndef PLAYROUND_H_INCLUDED
#define PLAYROUND_H_INCLUDED

#include "../../lists/list.h"
#include "../../player/player.h"

int PlayRound(S_list *, S_list *, S_player *);

/*
	Play a round and modify player
	Need a list of wrestlers to use
	A list of status to apply
	A player to edit
	Return 1 if wants to save and 0 if not
*/

#endif // PLAYROUND_H_INCLUDED