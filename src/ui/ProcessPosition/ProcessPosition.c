/*
* @Author: klmp200
* @Date:   2016-05-23 22:16:40
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-23 22:17:16
*/

#include <stdio.h>
#include <stdlib.h>

int ProcessPosition(int n, int size){

	if (n < 0){

		n = size - ((-n) % size);

		if (n == size){

			n = 0;

		}

	} else {

		n = n % size;

	}

	return n;
}