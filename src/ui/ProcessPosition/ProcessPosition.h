#ifndef PROCESSPOSITION_H_INCLUDED
#define PROCESSPOSITION_H_INCLUDED

int ProcessPosition(int, int);

/*
	Return the position in a menu
	Need the current position and a size to match with
*/

#endif // PROCESSPOSITION_H_INCLUDED