/*
* @Author: klmp200
* @Date:   2016-05-23 09:03:23
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-25 23:01:10
*/

#include <stdio.h>
#include <stdlib.h>
#include "../getcurses.h"
#include "../ProcessPosition/ProcessPosition.h"

int WDisplayMenu(WINDOW *win, int n, char *menu[], int size){
	int i;

	n = ProcessPosition(n, size);

	wmove(win, 0, 0);
	wclear(win);
	wrefresh(win);

	for (i = 0; i < size; i++){

		if (n == i){

			wattron(win, A_REVERSE);
			wprintw(win, "%s\n", menu[i]);
			wattroff(win, A_REVERSE);

		} else {

			wprintw(win, "%s\n", menu[i]);

		}

	}

	wrefresh(win);

	return n;
}
