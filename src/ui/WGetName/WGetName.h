#ifndef WGETNAME_H_INCLUDED
#define WGETNAME_H_INCLUDED

#include "../getcurses.h"
#include "../../player/player.h"

#define GetName(player) (WGetName(stdscr, player))

void WGetName(WINDOW *, S_player *);

/*
	Ask a name to the player and put it in a S_player
*/

#endif // WGETNAME_H_INCLUDED