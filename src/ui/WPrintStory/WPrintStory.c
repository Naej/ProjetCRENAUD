/*
* @Author: klmp200
* @Date:   2016-06-01 00:00:49
* @Last Modified by:   klmp200
* @Last Modified time: 2016-06-03 09:07:25
*/

#include <stdio.h>
#include <stdlib.h>

#include "../getcurses.h"

void WPrintStory(WINDOW *win, int min, int max){

	int ch;

	char *story[] = {
		"Vendredi 3 juin 2016, le cours d'IFC commence dans une heure. Vite, vite, je m'habille et je descend prendre mon cafe.\nBonjour papa, bonjour maman !\n - \"Salut fiston. Nous pouvons t'amener en cours aujourd'hui, notre prochaine experience en laboratoire a ete reporte a cette apres-midi.\"\nJ'admire beaucoup mes parents, ils sont chercheurs au CERN, j'espere devenir chercheur moi aussi.",
		"Je rentre dans la voiture.",
		"*BAAAAM !!!*",
		"Un portail spatio-tempo-dimensionnel vient d'apparaitre au milieu du garage !",
		"*AHHHHHHHHHHHHH !!!*",
		"\"Maman, Papa ! Que se passe t-il ? Qui sont ces gens ?\"",
		"Mes parents n'ont pas le temps de repondre que nous nous sommes deja fait capturer.\nNous traversons le portail et je suis jete dans un vieux donjon medieval sans mes parents.",
		"- Psst, petit, rapproche toi !\n- Mais qui etes vous ?\n- Je suis Napoleon Malaparte, j'imagine que tu te demandes ce que tu fais ici.\n- J'ai perdu mes parents.\"",
		"Napoleon m'explique alors toute l'histoire de ce monde (voir le document sur l'histoire fourni avec le programme).\nApparemment, j'ai ete enleve par une ligue de catch nazi, actuellement au pouvoir dans ce monde ou les ligues de catch controlent le monde.",
		"- Enfuyons nous par le tunnel que j'ai creuse avec une cuillere pendant 20 ans et rejoignons la Resistance.\nJe le suis sans hesiter et nous arrivons a sortir du donjon.",
		"- Nouvelle France, QG de la Nouvelle Resistance, Annee du Grand Ragondin Cosmique -\nNapoleon et ses amis, Guillaume le goeland et Alexandre le plus Grand elaborent avec moi un plan pour contrecarrer les Nazis et sauver mes parents.",
		"- \"C'est tres simple. Me dit Guillaume. Nous n'avons qu'a creer notre propre ligue de catch et remporter le tournoi dans les annees qui viennent. Tu seras notre chef\n- Mais je ne sais jouer qu'au solitaire !\n- Ce n'est pas grave, tu es l'Elu.\"",
		"\"Tu as jusqu'a l'annee du Caribou des plaines pour reussir. Chaque annee tu devra embaucher des catcheurs avec ton budget et les faire jouer ensemble pour obtenir le plus de popularite pour notre ligue.\nBonne chance !\"\n",

		"Papa, maman, je vous ai sauve !\n- Non. Me dit mon papa. C'est moi qui dirige la ligue nazi. T'es vraiment un sale gosse, tu as fait rater notre plan a ta mere et moi.",
		"NOOOOOOOOOOOOOOOOOOOOOOOOON !",
		"Et oui, durant mon travail au CERN, j'ai reussi a entrer en contact avec ce monde parallel. Toujours restreins par notre budget, les nazis representaient une source de financement tres interessante, alors nous les avons rejoins. Il ne suffisait plus que de nous debarasser de toi !",
		"Mais c'est horrible ! Ces gens sont mechants et moi je suis gentil.",
		"A suivre...",
	};

	noecho();
	cbreak();
	wmove(win, 0, 0);

	do {
		wclear(win);
		wprintw(win, "%s", story[min]);
		wrefresh(win);
		min++;
	} while ((ch = getch()) != 27 && min <= max);


}
