#ifndef WPRINTSTORY_H_INCLUDED
#define WPRINTSTORY_H_INCLUDED

#include "../getcurses.h"

#define PrintStory(min, max) (WPrintStory(stdscr, min, max))

void WPrintStory(WINDOW*, int, int);

/*
	Prints dialogs from min to max in a given window
*/

#endif // WPRINTSTORY_H_INCLUDED