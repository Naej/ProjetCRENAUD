/*
* @Author: klmp200
* @Date:   2016-06-01 08:42:35
* @Last Modified by:   klmp200
* @Last Modified time: 2016-06-02 21:51:11
*/

#include <stdio.h>
#include <stdlib.h>

#include "../../lists/list.h"
#include "../../status/status.h"
#include "../S_wrestler.h"

void ApplyRandomEffect(S_list *sList, S_wrestler *wrestler){

	int i = 0;

	while (i < MAX_W_STATUS && wrestler->status[i] != NULL){

		i++;

	}

	if (i < MAX_W_STATUS && wrestler->status[i] == NULL){

		wrestler->status[i] = StatusCpy((S_status*)RndList(sList)->data);

	}


}