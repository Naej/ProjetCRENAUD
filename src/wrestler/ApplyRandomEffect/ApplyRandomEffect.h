#ifndef APPLYRANDOMEFFECT_H_INCLUDED
#define APPLYRANDOMEFFECT_H_INCLUDED

#include "../../lists/list.h"
#include "../S_wrestler.h"

void ApplyRandomEffect(S_list *, S_wrestler *);

/*
	Apply a random effect on a wrestler
	srand should be launched before calling the function
	Need a list of status and a wrestler
*/

#endif // APPLYRANDOMEFFECT_H_INCLUDED