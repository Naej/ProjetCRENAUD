/*
* @Author: klmp200
* @Date:   2016-05-30 15:47:32
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-30 16:33:23
*/

#include <stdio.h>
#include <stdlib.h>

#include "../../lists/list.h"
#include "../S_wrestler.h"

S_list * ConcatFastWrestlerList(S_list *list1, S_list *list2){

	S_element *element = NULL;
	S_element *elementTmp = NULL;

	if (list1 != NULL && list2 != NULL){

		element = list2->first;

		while (element != NULL){

			AppendList(list1, element->data, sizeof(S_wrestler));
			elementTmp = element->next;
			PopPtnList(list2, element);
			element = elementTmp;

		}

		FreeList(list2);

	}

	return list1;
}