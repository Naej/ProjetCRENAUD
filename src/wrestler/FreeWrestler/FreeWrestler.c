/*
* @Author: klmp200
* @Date:   2016-05-16 17:03:51
* @Last Modified by:   klmp200
* @Last Modified time: 2016-05-22 00:29:51
*/

#include <stdio.h>
#include <stdlib.h>

#include "../S_wrestler.h"

int FreeWrestler(S_wrestler * wrestler){
	int ok = 1;
	int i;

	if (wrestler != NULL){

		if (wrestler->name != NULL){
			free(wrestler->name);
			wrestler->name = NULL;
		}
		if (wrestler->description != NULL){
			free(wrestler->description);
			wrestler->description = NULL;
		}
		if (wrestler->ascii != NULL){
			free(wrestler->ascii);
			wrestler->ascii = NULL;
		}

		wrestler->price = 0;
		wrestler->fame = 0;
		wrestler->strength = 0;

		/* Free Status here */

		for (i=0;i<MAX_W_STATUS;i++){

			if (wrestler->status[i] != NULL){

				free(wrestler->status[i]);
				wrestler->status[i] = NULL;

			}

		}

		ok = 0;
	}

	return ok;
}