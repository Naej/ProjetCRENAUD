#ifndef SWRESTLER_H_INCLUDED
#define SWRESTLER_H_INCLUDED

#define MAX_W_STATUS 5

#include "../status/S_status.h"

typedef struct S_wrestler S_wrestler;
struct S_wrestler {
	char * name;
	char * description;
	char * ascii;
	int price;
	int fame;
	int strength;
	S_status *status[MAX_W_STATUS];
};

#endif // SWRESTLER_H_INCLUDED